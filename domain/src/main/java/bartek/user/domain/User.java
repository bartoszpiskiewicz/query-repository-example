package bartek.user.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@NoArgsConstructor(access = AccessLevel.PACKAGE, force = true)
@AllArgsConstructor
@Getter
class User {

    @Id
    @GeneratedValue
    private final long id;

    private String name;

    private int age;

    private UUID token;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Role> roles;

}
