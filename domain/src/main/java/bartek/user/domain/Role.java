package bartek.user.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@AllArgsConstructor
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@Getter
class Role {

    static final String USER = "USER";
    static final String ADMIN = "ADMIN";

    @Id
    @GeneratedValue
    private long id;

    private final String name;

}
