package bartek.user.domain;

import bartek.common.repository.QueryRepositoryConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "bartek.user")
@EntityScan(basePackages = "bartek.user")
@Import(QueryRepositoryConfiguration.class)
public class UserConfiguration {

    @Bean
    UserService userService(UserRepository userRepository) {
        return new UserServiceImpl(userRepository);
    }

}
