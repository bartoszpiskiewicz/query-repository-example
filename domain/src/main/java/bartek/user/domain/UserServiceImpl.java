package bartek.user.domain;

import lombok.AllArgsConstructor;

import java.util.HashSet;
import java.util.UUID;

@AllArgsConstructor
class UserServiceImpl implements UserService{

    private final UserRepository userRepository;

    @Override
    public void createUser(String name, int age) {

        HashSet<Role> roles = new HashSet<>();
        roles.add(new Role(0, Role.USER));
        roles.add(new Role(0, Role.ADMIN));

        User user = new User(0, name, age, UUID.randomUUID(), roles);
        userRepository.save(user);
    }
}
