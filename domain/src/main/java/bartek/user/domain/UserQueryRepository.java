package bartek.user.domain;


import bartek.common.repository.QueryRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface UserQueryRepository extends QueryRepository<User, Long> {
}
