package bartek.user.rest.vm;

import org.springframework.beans.factory.annotation.Value;

import java.util.List;

public interface UserFlatDto {
    Long getId();

    String getName();

    int getAge();

    /**
     * Spring Expression Language
     * see: http://www.studytrails.com/frameworks/spring/spring-expression-language-access-list/
     **/
    @Value("#{target.roles.![name]}")
    List<String> getRoles();
}
