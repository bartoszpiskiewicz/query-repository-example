package bartek.user.rest.vm;


public interface UserBasicDto {

    Long getId();

    String getName();

    int getAge();

}
