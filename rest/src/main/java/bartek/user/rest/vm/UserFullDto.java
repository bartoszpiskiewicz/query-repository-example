package bartek.user.rest.vm;

import java.util.List;

public interface UserFullDto extends UserBasicDto {

    List<RoleDto> getRoles();

}
