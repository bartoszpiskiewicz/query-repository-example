package bartek.user.rest.controller;

import bartek.user.domain.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("{name}/{age}")
    public void createUser(@PathVariable String name, @PathVariable int age) {
        userService.createUser(name, age);
    }

    @PostConstruct
    public void init() {

        userService.createUser("Michał", 16);
        userService.createUser("Marcin", 22);
        userService.createUser("Roman", 15);
        userService.createUser("Wojtek", 12);
        userService.createUser("Jarek", 22);

    }


}
