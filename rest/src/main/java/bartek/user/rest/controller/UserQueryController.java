package bartek.user.rest.controller;

import bartek.user.rest.port.UserBasicDtoRepository;
import bartek.user.rest.port.UserFullDtoRepository;
import bartek.user.rest.vm.UserBasicDto;
import bartek.user.rest.vm.UserFlatDto;
import bartek.user.rest.vm.UserFullDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("user")
@AllArgsConstructor
public class UserQueryController {

    private final UserFullDtoRepository fullDtoRepository;
    private final UserBasicDtoRepository basicDtoRepository;

    @GetMapping("{id}")
    UserFullDto getById(@PathVariable long id) {
        UserFullDto userFullDto = fullDtoRepository.findById(id, UserFullDto.class).get();
        return userFullDto;
    }

    @GetMapping
    List<UserBasicDto> findAll() {
        List<UserBasicDto> users = basicDtoRepository.findAll(UserBasicDto.class);
        return users;
    }

    @GetMapping("admin")
    List<UserFullDto> findAdmins() {
        return fullDtoRepository.findAdmins();
    }

    @GetMapping("flat")
    List<UserFlatDto> findFlat() {
        return fullDtoRepository.findAllByAgeAfter(10);
    }

}
