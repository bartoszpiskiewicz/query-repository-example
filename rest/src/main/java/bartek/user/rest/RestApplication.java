package bartek.user.rest;

import bartek.common.repository.QueryRepositoryImpl;
import bartek.user.domain.UserConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "bartek")
@EnableJpaRepositories(repositoryBaseClass = QueryRepositoryImpl.class)
@Import(UserConfiguration.class)
public class RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApplication.class, args);
	}
}
