package bartek.user.rest.port;

import bartek.user.domain.UserQueryRepository;
import bartek.user.rest.vm.UserBasicDto;

import java.util.List;

public interface UserBasicDtoRepository extends UserQueryRepository {

    List<UserBasicDto> findAllByAgeAfter(int age);
}
