package bartek.user.rest.port;


import bartek.user.domain.UserQueryRepository;
import bartek.user.rest.vm.UserFlatDto;
import bartek.user.rest.vm.UserFullDto;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserFullDtoRepository extends UserQueryRepository {

    @Query("select u from User u join u.roles r where r.name='ADMIN'")
    List<UserFullDto> findAdmins();

    List<UserFlatDto> findAllByAgeAfter(int age);

}
