package bartek.common.repository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(repositoryBaseClass = QueryRepositoryImpl.class, basePackages = "bartek.common.repository")
public class QueryRepositoryConfiguration {
}
