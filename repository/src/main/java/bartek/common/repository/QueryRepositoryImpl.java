package bartek.common.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class QueryRepositoryImpl<T, ID> extends SimpleJpaRepository<T, ID> implements QueryRepository<T, ID> {

    private final ProjectionFactory projectionFactory;

    public QueryRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        projectionFactory = new SpelAwareProxyProjectionFactory();
    }

    @Override
    public <DTO> Optional<DTO> findById(ID id, Class<DTO> type) {
        Optional<T> data = this.findById(id);
        return data.map(t -> projectionFactory.createProjection(type, t));
    }

    @Override
    public boolean existsById(ID id) {
        return this.existsById(id);
    }

    @Override
    public long count() {
        return this.count();
    }

    @Override
    public <DTO> Page<DTO> findAll(Pageable pageable, Class<DTO> type) {
        return this.findAll(pageable)
                .map(t -> projectionFactory.createProjection(type, t));
    }

    @Override
    public <DTO> List<DTO> findAll(Class<DTO> type) {
        return this.findAll().stream()
                .map(t -> projectionFactory.createProjection(type, t))
                .collect(Collectors.toList());
    }

    @Override
    public <DTO> List<DTO> findAll(Sort sort, Class<DTO> type) {
        return this.findAll(sort).stream()
                .map(t -> projectionFactory.createProjection(type, t))
                .collect(Collectors.toList());
    }

    @Override
    public <DTO> List<DTO> findAllById(Iterable<ID> ids, Class<DTO> type) {
        return this.findAllById(ids).stream()
                .map(t -> projectionFactory.createProjection(type, t))
                .collect(Collectors.toList());
    }

}
