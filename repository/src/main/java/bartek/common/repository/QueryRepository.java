package bartek.common.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface QueryRepository<T, ID> extends Repository<T, ID> {

    <DTO> Optional<DTO> findById(ID id, Class<DTO> type);

    boolean existsById(ID id);

    long count();

    <DTO> Page<DTO> findAll(Pageable pageable, Class<DTO> type);

    <DTO> List<DTO> findAll(Class<DTO> type);

    <DTO> List<DTO> findAll(Sort sort, Class<DTO> type);

    <DTO> List<DTO> findAllById(Iterable<ID> ids, Class<DTO> type);

}
